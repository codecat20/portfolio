export const imageAssets = {
  collaborations: () => processImageAssets(collaborationsImages),
  webDesign: () => processImageAssets(webDesignImages),
};

const processImageAssets = (glob) =>
  Object.keys(glob).map((fp) => ({
    src: fp.replace("/public", ""),
    label: fp.split("/").pop().split(".")[0],
  }));

// import.meta.glob can only process string literals, thus the below is required
const collaborationsImages = import.meta.glob(
  "/public/assets/collaborations/*.{png,jpeg,jpg,PNG}"
);
const webDesignImages = import.meta.glob(
  "/public/assets/web-design/*.{png,jpeg,jpg,PNG}"
);
